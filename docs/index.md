# Home

Welcome to the [LogosBlock](https://logosblock.com/) API developer documentation. This document should contain all the information required to properly implement applications using our API.

At the momento we support one interface that can be used to access LogosBlock API:


| API | Version | Description |
| ------ | ------ | ------ |
| [WebSocket](wss/index.md) | v1.0.0 | Stateful API providing streaming of realtime data. |
