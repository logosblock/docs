# Support

The LogosBlock team is working hard to add new features, and support new exchanges APIs.

Any feedback or question about the platform:

- Send us an email at [API email](mailto:api@logosblock.com).
- Or visit our website [LogosBlock](https://logosblock.com/).
