# DotNET Examples

This is a very simple LogosBlock API WebSocket client example.

```html
<div>
using System;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using System.Text;
using Newtonsoft.Json;
using JWT.Builder;
using JWT.Algorithms;
using System.Linq;
using System.Collections.Generic;

namespace WSSClient {
    public class OutgoingMessage {
        public string op { get; set; }
        public string symbol { get; set; }
        public string channel { get; set; }
    }
    public class RequestResult {
        public bool success { get; set; }
        public string message { get; set; }
        public string code { get; set; }
    }
    class Program {
        public static void Main(string[] args) {
            // Websocket client instance creation.
            var socket = new ClientWebSocket();

            // Building the token through JWT with the seed and the clientUserName .
            var token = new JwtBuilder()
                .WithAlgorithm(new HMACSHA256Algorithm())
                .WithSecret("this-is-the-develop-seed")
                .AddClaim("exp", DateTimeOffset.UtcNow.AddHours(1).ToUnixTimeSeconds())
                .AddClaim("username", "logosblockusername")
                .Build();

            // Setting the request Header.
            // The header is set with  token like key and with the generated token like value.
            var tokenString = JsonConvert.SerializeObject(token);
            socket.Options.SetRequestHeader("token", token);

            // Building the outgoing message to subscribe to a channel and a symbol.
            OutgoingMessage om = new OutgoingMessage();
            om.op = "subscribe";
            om.channel = "tick";
            om.symbol = "btc_usd";
            string jsonStringMsg = JsonConvert.SerializeObject(om);
            byte[] message = Encoding.UTF8.GetBytes(jsonStringMsg);
            var sendBuffer = new ArraySegment<Byte>(message);
            string decMsg = Encoding.UTF8.GetString(message);

            CancellationTokenSource cts = new CancellationTokenSource();

            Task.Factory.StartNew(
                async () => {
                    try {
                        // Connecting with server.
                        Uri siteUri = new Uri(string.Format("ws://lbwebsocketserver.azurewebsites.net/ws/v1"));
                        await socket.ConnectAsync(siteUri, cts.Token);
                        Console.WriteLine("The client has connected to the host: {0}", siteUri.Host);
                        // Sending the susbcription message. 
                        await socket.SendAsync(sendBuffer, WebSocketMessageType.Text, endOfMessage: true, cancellationToken: cts.Token);

                        while (true) {
                            // Getting Messages throught an ArraySegment.
                            var rcvBytes = new byte[1024];
                            var rcvBuffer = new ArraySegment<byte>(rcvBytes);
                            WebSocketReceiveResult rcvResult = await socket.ReceiveAsync(rcvBuffer, cts.Token);
                            // Encoding the incoming data. 
                            byte[] msgBytes = rcvBuffer.Skip(rcvBuffer.Offset).Take(rcvResult.Count).ToArray();
                            string rcvMsg = Encoding.UTF8.GetString(msgBytes);

                            if (rcvMsg.Contains("ping")) {
                                Console.WriteLine("Received Ping: {0}", rcvMsg);
                                // Deserialization the received ping message.
                                RequestResult requestResult;
                                requestResult = JsonConvert.DeserializeObject<RequestResult>(rcvMsg);
                                if (requestResult.message != null) {
                                    // Building outcomming message.   
                                    OutgoingMessage pingResponse = new OutgoingMessage();
                                    pingResponse.op = "pong";
                                    pingResponse.channel = null;
                                    // Serialization, coding and message delivery.   
                                    jsonStringMsg = JsonConvert.SerializeObject(pingResponse);
                                    message = Encoding.UTF8.GetBytes(jsonStringMsg);
                                    sendBuffer = new ArraySegment<Byte>(message);
                                    decMsg = Encoding.UTF8.GetString(message);
                                    await socket.SendAsync(sendBuffer, WebSocketMessageType.Text, endOfMessage: true, cancellationToken: cts.Token);
                                    Console.WriteLine("Message sent:  {0}", decMsg);
                                }
                            } else {
                                Console.WriteLine("Received Message: {0}", rcvMsg);
                            }
                        }
                    } catch (Exception e) {
                        Console.WriteLine("Exception: {0} ", e);
                    }
                },
                cts.Token,
                TaskCreationOptions.LongRunning,
                TaskScheduler.Default
            ).Wait();
            Console.ReadKey(true);
        }
    }
}
</div>
```
