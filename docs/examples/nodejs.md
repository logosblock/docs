# NodeJS Examples

This is a very simple LogosBlock API WebSocket client example.

```js
const jwt = require('jsonwebtoken');
const WebSocket = require('ws');

const wssEndpoint = 'logosblock-websocket-endpoint';
const username = 'myusername';
const expiresIn = 60 * 60;

const token = jwt.sign({ username: username }, 'this-is-the-develop-seed', { expiresIn: expiresIn });
const options = {
    headers: {
        token: token
    }
};

const ws = new WebSocket(wssEndpoint, options);

ws.on('open', function() {
    console.log('The websocket is connected to the endpoint.', wssEndpoint);
    console.log('Subscribing to btc_usd / tick.');
    ws.send(JSON.stringify({
        op: 'subscribe',
        symbol: 'btc_usd',
        channel: 'tick'
    }));
});

ws.on('error', error => {
    console.log(`Error: ${error}.`);
    process.exit(-1);
});
ws.on('close', (code, reason) => {
    console.log(`Connection was close. Code: ${code} - Reason: ${reason}`);
    process.exit(-1);
});

ws.on('message', function(event) {
    const message = JSON.parse(event);
    console.log(message);
    if (message.message && message.message === 'ping') {
        ws.send(JSON.stringify({
            op: 'pong',
        }));
    };
});
```
