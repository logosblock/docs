# Python Examples

This is a very simple LogosBlock API WebSocket client example.

```python
# README:
#   https://docs.python-guide.org/dev/virtualenvs/
# -*- coding: utf-8 -*-
from ws4py.client.threadedclient import WebSocketClient
import json
import datetime

wssEndpoint = 'logosblock-websocket-endpoint'

class EchoClient(WebSocketClient):
    def opened(self):
        print('Connected to WSS endpoint: {}.'.format(wssEndpoint))
        self.send(json.dumps({
            'op': 'subscribe',
            'symbol': 'btc_usd',
            'channel': 'tick'
        }))

    def closed(self, code, reason):
        print(("Closed down", code, reason))

    def received_message(self, m):
        msg = json.loads(str(m))
        print(msg)
        if msg.get('message') == 'ping':
            self.send(json.dumps({
                'op': 'pong',
            }))

if __name__ == '__main__':
    try:
        ws = EchoClient(wssEndpoint, protocols=['http-only', 'chat'])
        ws.daemon = False
        print('Connecting to WSS endpoint: {}.'.format(wssEndpoint))
        ws.connect()
    except KeyboardInterrupt:
        ws.close()


```
