# WebSocket API

## Overview

Our WebSocket endpoint provides real-time market data streaming which works in subscribe / publish communication model. After establishing a WebSocket connection with us, you will need to send a ***subscription*** message documented below.

If everything is correct, we will provide you with a continuous stream of real-time market data updates.

> The websocket client implementation must comply with the `v13` version of the protocol documented in the [RFC6455](https://www.ietf.org/rfc/rfc6455.txt), including responding with the `Pong` message each time we will send `Ping`, failure to respond with the `Pong` from your WebSocket client will cause a connection to be closed. More details in secion ```Heartbeat Message```

### Supported Exchanges

Our WebSocket API is connected with the following exchanges API:

- BiBox
- Bitfinex
- BitMex
- BitStamp
- Coinbase
- CryptoFacilities
- DigiFinex
- GateIO
- HitBTC
- Huobi
- Kraken
- OkEx

We are working hard to integrate these other exchanges:

- Binance
- SeedCX
- ZB.com
- Others

### WebSocket API Endpoint

Our WebSocket API is exposed in the URL below:

```ws://wssproduction.azurewebsites.net/ws/v1/```

## Messages

### Interacting with WebSocket API

Each message sent to the WebSocket API must be in a string format. It means the JSON message described in each section must be *`stringified`* using a library/dependency like `JSON.stringify(message)` in NodeJS (or similar in other languages).

Example in NodeJS:

```js
    const ws = ...// Create WebSocket client instance.
    ... // ws initialization

    const jsonMessage = {
        op: 'subscribe',
        pair: 'btc_usd',
        channel: 'tick'
    };
    // Stringify the JSON message.
    const stringMessage = JSON.stringify(jsonMessage);
    // Sending the message as string.
    ws.send(stringMessage);

```

> In case sending a PONG message using the `.pong` function, it is very similar to the `.send` example.

### Authentication Success

After connecting to the WSS endpoint, you will receive a success authentication message:

```json
{
    success: true,
    message: 'Authentication successful',
    code: '500'
}
```

### Subscribe

After a success connection, you will able to subscribe to any of the available channels:

- Ticks
- Orders
- Status
- VWAP

### Ticks

#### Tick Subscribe

```json
    {
        op: 'subscribe',
        symbol: 'btc_usd',
        channel: 'tick'
    }
```

#### Success Response

```json
{
    success: true,
    message: 'Your subscription to tick / btc_usdt was success.'
}
```

#### Tick Data Response

```json
    {
        timestamp: 1574954051862,
        channel: 'tick',
        pair: 'btc_usdt',
        data: [
            {
                exchange: 'ExchangeA',
                symbol: 'BTC_USDT',
                price: 7483.76,
                side: 'sell',
                amount: 0.0004,
                timestamp: 1574954051000,
                createdAt: 1574954051862,
                tradeId: '210894055',
                extraData: {
                    ...
                }
            }
            ...
            ...
        ]
    }
```

> The *extraData* field contains any other value sent by the exchange.

#### Error: Pair not supported for channel Tick

````json
    {
        success: false,
        errorCode: 4502,
        reason: 'The channel/pair tick/btc_usdt is not supported.'
    }
````

### Orders

#### Order Subscribe

```json
    {
        op: 'subscribe',
        symbol: 'btc_usd',
        channel: 'order'
    }
```

#### Success Order Subscription

```json
{
    success: true,
    message: 'Your subscription to order / btc_usd was success.'
}
```

#### Order Data Response

```json
    {
        timestamp: 1574959868183,
        channel: 'orderbook',
        pair: 'btc_usd',
        data: [
            {
                exchange: 'ExchangeA',
                symbol: 'BTCUSD',
                price: 0,
                amount: 1,
                type: 'Ask',
                createdAt: 1574959868183,
                extraData: {}
            },
            ...
            ...
        ]
    }
```

#### Error: Pair not supported for channel Order

````json
    {
        success: false,
        errorCode: 4502,
        reason: 'The channel/pair order/btc_usd is not supported.'
    }
````

### Status

#### Status Subscribe

```json
    {
        op: 'subscribe',
        symbol: '', // empty string
        channel: 'status'
    }
```

#### Success Status Subscription

```json
    {
        success: true,
        message: 'Your subscription to status /  was success.'
    }
```

#### Status Message

```json
    {
        timestamp: 1574888890634,
        channel: 'status',
        pair: '',
        data: [
            {
                exchange: 'ExchangeA',
                status: 'connected',
                isConnected: true
            },
            {
                exchange: 'ExchangeB',
                status: 'connected',
                isConnected: true
            }
        ]
    }
```

### VWAP

#### VWAP Subscribe

```json
    {
        op: 'subscribe',
        symbol: 'btc_usd`,
        channel: 'vwap
    }
```

#### Success VWAP Subscription

```json
    {
        success: true,
        message: 'Your subscription to vwap / btc_usd was success.'
    }
```

#### VWAP Data Response

```json
    {
        timestamp: 1574959992453,
        channel: 'vwap',
        pair: 'btc_usd',
        data: {
            vwap: 7610.127622244841,
            exchange: 'ExchangeA',
            candles: [ ... ],
            lastCandle: {
                timestamp: 1574959920000,
                high: 7614.7,
                low: 7603.23,
                open: 7604.43,
                close: 7614.7,
                volume: 0.46169796,
                vwap: 7610.127622244841,
                ticks: [ ... ]
            }
        }
    }
```

#### Error: Pair not supported for channel VWAP

````json
    {
        success: false,
        errorCode: 4502,
        reason: 'The channel/pair order/btc_usd is not supported.'
    }
````

### Heartbeat

The WebSocket API implements a Heartbeat mechanics. The API sends a PING message periodically. When a client receives the PING message, it must reply it, sending a PONG message. If a client doesn't reply two PING message, its connection will be closed.

#### Using `.pong` vs `.send` functions

Depending on the WebSocket client implementation, it may support sending a PONG message using `.pong` or `.send` function.

The LogosBlock WebSocket API supports both functions. It means the WebSocket client implementation can reply/send the PONG message using any of both functions.

In both cases, the PONG message must be as below:

#### Ping Message

```json
    {
        success: true,
        message: 'ping',
        code: '500'
    }
```

#### Pong Message Response Example

```json
    {
        op: 'pong',
    }
```
